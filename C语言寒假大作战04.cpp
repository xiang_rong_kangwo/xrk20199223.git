#include<stdio.h>
#include<stdlib.h>
void operation1();
void operation2();
void operation3();
void menu();
void help();
void error();
int main()
{
    int Grade=1;
    printf("========口算生成器========\n欢迎使用口算生成器：)\n\n");
    help();
    menu(); 
    while(Grade!=5){
        scanf("%d",&Grade);
        printf("<执行操作:)\n\n");
        switch(Grade){
            case 1:operation1();break;
            case 2:operation2();break;
            case 3:operation3();break;
            case 4:help();break;
            case 5:printf("程序结束，欢迎下次使用  任意键结束......\n");break;
            default:error();break;
        }if(Grade!=5) menu();
    }
    return 0;
 } 
void operation1(){
    int a,b,i;
    printf("现在是一年级题目：\n请输入生成个数>");
    scanf("%d",&a);
    for(i=0;i<a;i++){
        int yi=rand()%11;
        int er=rand()%11;
        char c[2]={'+','-'};
        b=rand()%2;
        if(c[b]=='+'){
            printf("%2d %c %2d= %d\n",yi,c[b],er,yi+er);    
        }
        else{
            printf("%2d %c %2d= %d\n",yi,c[b],er,yi-er);
        } 
    }
}
void operation2(){
    int a,b,i;
    printf("现在是二年级题目：\n请输入生成个数>");
    scanf("%d",&a);
    for(i=0;i<a;i++){
        double yi=rand()%101;
        double er=rand()%101;
        double san=rand()%100+1;
        char c[2]={'*','/'};
        b=rand()%2;
        if(c[b]=='/')printf("%2g / %2g= %g\n",yi,san,yi/san);
        else printf("%2g * %2g= %g\n",yi,er,yi*er);
    }
}
void operation3(){
    int a,b,i,b1;
    printf("现在是三年级题目：\n请输入生成个数>");
    scanf("%d",&a);
    for(i=0;i<a;i++){
        double yi=rand()%101;
        double er=rand()%101;
        double san=rand()%101;
        double si=rand()%100+1;
        char c[4]={'+','-','*','/'};
        b=rand()%4;
        b1=rand()%4;
        if(c[b]=='*'){
            switch(c[b1]){
                case '-':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi*er-san);break; 
                case '+':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi*er+san);break;
                case '/':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],si,yi*er/si);break;
                case '*':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi*er*san);break;
            }
        } 
        if(c[b]=='+'){
            switch(c[b1]){
                case '-':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi+er-san);break;
                case '+':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi+er+san);break;
                case '/':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],si,yi+er/si);break;   
                case '*':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi+er*san);break;
            }
        } 
        if(c[b]=='-'){
            switch(c[b1]){
                case '-':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi-er-san);break;
                case '+':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi-er+san);break;
                case '/':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],si,yi-er/si);break;   
                case '*':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],er,c[b1],san,yi-er*san);break;
            }
        } 
        if(c[b]=='/'){
            switch(c[b1]){
                case '-':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],si,c[b1],san,yi/si-san);break; 
                case '+':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],si,c[b1],san,yi/si+san);break;
                case '/':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],si,c[b1],si,yi/si/si);break;   
                case '*':
                    printf("%2g %c %2g %c %2g= %g\n",yi,c[b],si,c[b1],san,yi/si*san);break; 
            }
        } 
    }
}
void menu(){
    printf("操作列表：\n1)一年级    2)二年级    3)三年级\n4)帮助     5)退出程序\n请输入操作>");
}
void help(){
    printf("帮助信息:\n您需要输入命令代号来进行操作，且\n一年级题目为不超过十位的加减法；\n二年级题目为不超过百位的乘除法；\n三年级题目为不超过百位的加减乘除混合题目.\n\n");
}
void error(){
    printf("Error!!!\n错误操作指令，请重新输入\n\n"); 
} 
