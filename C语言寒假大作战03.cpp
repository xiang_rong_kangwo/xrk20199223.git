#include<stdio.h>
#include<stdlib.h>
void operation1(){
    int a,b,i;
    printf("现在是一年级题目：\n请输入生成个数>");
    scanf("%d",&a);
    for(i=0;i<a;i++){
        int shu1=rand()%11;
        int shu2=rand()%11;
        char fh[2]={'+','-'};
        b=rand()%2;
        printf("%2d %c %2d=___\n",shu1,fh[b],shu2);
    }
} 
void operation2(){
    int a,b,i;
    printf("现在是二年级题目：\n请输入生成个数>");
    scanf("%d",&a);
    for(i=0;i<a;i++){
        int shu1=rand()%101;
        int shu2=rand()%101;
        int shu3=rand()%100+1;
        char fh[2]={'*','/'};
        b=rand()%2;
        if(fh[b]=='/')printf("%2d %c %2d=___\n",shu1,fh[b],shu3);
        else printf("%2d %c %2d=___\n",shu1,fh[b],shu2);
    }
}
void operation3(){
    int a,b,i,b1;
    printf("现在是三年级题目：\n请输入生成个数>");
    scanf("%d",&a);
    for(i=0;i<a;i++){
        int shu1=rand()%101;
        int shu2=rand()%101;
        int shu3=rand()%101;
        int shu4=rand()%100+1;
        char fh[4]={'+','-','*','/'};
        b=rand()%4;
        b1=rand()%4;
        if(fh[b]=='/'&&shu2==0) shu2=shu4;
        if(fh[b1]=='/'&&shu3==0) shu3=shu4;
        printf("%2d %c %2d %c %2d=___\n",shu1,fh[b],shu2,fh[b1],shu3);
        
    }
}
void menu(){
    printf("操作列表：\n1)一年级    2)二年级    3)三年级\n4)帮助     5)退出程序\n请输入操作>");
} 
void help(){
    printf("帮助信息:\n您需要输入命令代号来进行操作，且\n一年级题目为不超过十位的加减法；\n二年级题目为不超过百位的乘除法；\n三年级题目为不超过百位的加减乘除混合题目.\n\n");
} 
void error(){
    printf("Error!!!\n错误操作指令，请重新输入\n\n"); 
}
int main(){
    int Grade=1;
    printf("========口算生成器========\n欢迎使用口算生成器：)\n\n");
    help();
    menu();
    while(Grade!=5){
        scanf("%d",&Grade);
        printf("<执行操作:)\n\n");
        switch(Grade){
            case 1:operation1();break;
            case 2:operation2();break;
            case 3:operation3();break;
            case 4:help();break;
            case 5:printf("程序结束，欢迎下次使用  任意键结束......\n");break;
            default:error();break;
        }if(Grade!=5) menu();
    }
    return 0;
}
