#include<stdio.h>
void menu(){
    printf("操作列表：\n1)一年级    2)二年级    3)三年级\n4)帮助     5)退出程序\n请输入操作>");
} 
void GradeOne(){
    printf("现在是一年级题目:3+4=\n\n\n");
}
void GradeTwo(){
    printf("现在是二年级题目:41*2=\n\n\n");
}
void GradeThree(){
    printf("现在是三年级题目:11+24*2=\n\n\n");
} 
void help(){
    printf("帮助信息:\n您需要输入命令代号来进行操作，且\n一年级题目为不超过十位的加减法；\n二年级题目为不超过百位的乘除法；\n三年级题目为不超过百位的加减乘除混合题目.\n\n");
} 
void error(){
    printf("Error!!!\n错误操作指令，请重新输入\n\n"); 
}
int main(){
    int Grade=1;
    printf("========口算生成器========\n欢迎使用口算生成器：)\n\n");
    help();
    menu();
    while(Grade!=5){
        scanf("%d",&Grade);
        printf("<执行操作:)\n\n");
        switch(Grade){
            case 1:GradeOne();break;
            case 2:GradeTwo();break;
            case 3:GradeThree();break;
            case 4:help();break;
            case 5:printf("程序结束，欢迎下次使用  任意键结束......\n");break;
            default:error();break;
        }if(Grade!=5)menu();
    }
    return 0;
}
