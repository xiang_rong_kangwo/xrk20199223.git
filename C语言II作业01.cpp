#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
int all = 0;
int max = 50;
struct imfor
{
	int id=0;
	char name[100];
	char pho[100];
}imfor[50],t;

//查询id是否存在
int find(int id) {
	int index = -1;
	for (int i = 0; i < all; i++) {
		if (imfor[i].id == id) {
			index = i;
		}
	}
	return index;
}

void sort(void){//排序界面
	int i;
	printf("\n\n-----------用户排序------------\n");
	printf("1）编号排序  2）姓名排序\n ");
	printf("请选择排序的方式\n ");
	scanf("%d", &i);
	if (i == 1) {
		for (int k = 0; k < all - 1; k++)
			for (int j = 0; j < all - i - 1; j++)
				if (imfor[j].id > imfor[j + 1].id) {
					t = imfor[j];
					imfor[j] = imfor[j + 1];
					imfor[j + 1] = t;
				}
		for (int a = 0; a < all; a++) {
			printf("%d \t%s \t%s\n", imfor[a].id, imfor[a].name,imfor[a].pho);
		}
	}
	else if (i == 2) {
		for (int i = 0; i < all - 1; i++)
		{
			for (int j = 0; j < all - 1 - i; j++)
			{
				if (strcmp(imfor[j].name, imfor[j].name) < 0)
				{
					t = imfor[j];
					imfor[j] = imfor[j + 1];
					imfor[j + 1] = t;
				}
			}
		}
		for (int a = 0; a < all; a++) {
			printf("%d \t%s \t%s\n", imfor[a].id, imfor[a].name, imfor[a].pho);
		}
	}
	else
		printf("error!\n");
}
int add() {//添加界面
	printf("\n\n-----------添加用户------------\n");
	if (all == 50) {
		printf("通讯录已满");
		printf("回车继续");
		getchar();
		return 0;
	}
	printf("\n编号：\n");
	scanf("%d", &imfor[all].id);
	if (find(imfor[all].id) != -1) {
		printf("错误:编号重复");
		getchar();
		return 0;
	}
	printf("\n姓名：\n");
	scanf("%s", imfor[all].name);
	getchar();
	printf("\n电话：\n");
	scanf("%s", imfor[all].pho);
	getchar();
	printf("\n-----------添加完成------------\n");
	all++;
}

//删除
int del2(int id) {
	int index = find(id);
	if (index == -1) {
		return 0;
	}
	else {
		for (int i = index + 1; i < all; i++) {
			imfor[i - 1] = imfor[i];
		}
		all--;
		return 1;
	}
}
//删除界面
void del() {
	int id;
	char choose;
	printf("\n-----------删除用户------------\n");
	printf("\n请选择待删除用户编号（-1退出）\n");
	scanf("%d", &id);
	getchar();
	if (id == -1) {
		printf("你放弃了删除\n");
		return;
	}
	printf("\n确认是否删除（Y/N）:\n");
	scanf("%c", &choose);
	getchar();
	if (choose == 'y' || 'Y') {
		if (del2(id) == 0) {
			printf("删除失败，id不存在\n");
			getchar();
		}
		else
			printf("删除成功\n");
		getchar();
	}
	else {
		printf("取消删除\n");
		getchar();
	}
}

int change2(int id) {//修改
	printf("\n编号：\n");
	scanf("%d", &imfor[all].id);
	if (find(imfor[all].id) != -1) {
		printf("错误:编号重复");
		printf("回车继续");
		getchar();
		return 0;
	}
	printf("\n姓名：\n");
	scanf("%s", imfor[all].name);
	printf("\n电话：\n");
	scanf("%s", imfor[all].pho);
	all++;
	printf("-----------修改完成------------\n");
	getchar();
}

void change() {//修改界面
	int id;
	char choose;
	printf("\n-----------修改用户------------\n");
	printf("请选择待修改用户编号\n");
	scanf("%d", &id);
	getchar();
	if (find(id) == -1) {
		printf("错误:id不存在\n");
		printf("回车继续");
		getchar();
	}
	else {
		del2(id);
		change2(id);
	}
}


void lookup() {//查找界面
	int a, flag = 0;
	char b[100];
	printf("\n-----------查找用户------------");
	printf("\n1)姓名查找  2)电话查找\n");
	scanf("%d", &a);
	if (a == 1) {
		printf("\n请输入用户姓名:\n");
		scanf("%s", b);
		for (int i = 0; i < all; i++) {
			if (strcmp(imfor[i].name, b) == 0) {
				printf("%d \t\t%s \t\t%s\n", imfor[i].id, imfor[i].name, imfor[i].pho);
				printf("回车继续");
				getchar();
				flag = 1;
			}
		}
	}
	if (a == 2) {
		printf("\n请输入用户号码:\n");
		scanf("%s", b);
		for (int i = 0; i < all; i++) {
			if (strcmp(imfor[i].pho, b) == 0) {
				printf("%d \t\t%s \t\t%s\n", imfor[i].id, imfor[i].name, imfor[i].pho);
				printf("回车继续");
				getchar();
				flag = 1;
			}
		}
	}
	if (flag == 0) {
		printf("数据不存在");
		printf("回车继续");
	}
	getchar();
}


void menu() {//菜单

	printf("========== 通讯录 ==========\n\n");
	printf("========== 界面 ==========\n");
	printf("人数： %d  人           | 剩余空间： %d  人\n",all,max-all);
	printf("编号:  ___ | 姓名: ____ | 电话: ____\n\n\n");
	for (int a = 0; a < all; a++) {
		printf("%d \t\t%s \t\t%s\n", imfor[a].id, imfor[a].name, imfor[a].pho);
	}
	printf("\n\n\n\n操作列表:\n");
	printf("1)排序          2)添加          3)删除\n");
	printf("4)修改          5)查找          6)退出程序\n");

}
int main()
{
	menu();
	int n = 0;
	while (n != 6) {
		printf("请输入操作:\n ");
		scanf("%d", &n);
		switch (n)
		{
		case 1:sort(); system("cls"); menu(); break;//排序
		case 2:add(); system("cls"); menu(); break;//添加
		case 3:del(); system("cls"); menu(); break;//删除
		case 4:change(); system("cls"); menu(); break;//修改
		case 5:lookup(); system("cls"); menu(); break;//查找
		default:printf("退出程序\n");break;
		}
	}
	return 0;
}
